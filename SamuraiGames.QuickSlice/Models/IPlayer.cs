﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.QuickSlice.Models
{
    public interface IPlayer
    {
        int id { get; set; }

        string playerName { get; set; }

        bool isAI { get; }
    }
}
