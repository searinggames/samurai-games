﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.QuickSlice.Models
{
    public class HumanPlayer : IPlayer
    {
        public int id { get; set; }

        public string playerName { get; set; }

        public bool isAI { get { return false; } }
    }
}
