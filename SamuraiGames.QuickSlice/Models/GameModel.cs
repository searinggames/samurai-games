﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.QuickSlice.Models
{
    public class GameModel : IGameModel
    {
        public List<IPlayer> players { get; set; }

        public int numRounds { get; set; }       
        
        public List<IRoundModel> rounds { get; set; } 
    }
}
