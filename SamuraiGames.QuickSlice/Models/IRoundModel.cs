﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.QuickSlice.Models
{
    public interface IRoundModel
    {
        int winningPlayerId { get; set; }

        int losingPlayerId { get; set; }
    }
}
