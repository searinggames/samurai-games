﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.QuickSlice.Models
{
    public class RoundModel : IRoundModel
    {
        public int winningPlayerId { get; set; }

        public int losingPlayerId { get; set; }
    }
}
