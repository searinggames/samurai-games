﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.QuickSlice.Models
{
    public interface IGameModel
    {
        List<IPlayer> players { get; set; }

        int numRounds { get; set; }
    }
}
