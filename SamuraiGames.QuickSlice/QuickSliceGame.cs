﻿using SamuraiGames.Core;
using SamuraiGames.Core.Events;
using SamuraiGames.Core.Game;
using SamuraiGames.Core.Minigames;
using SamuraiGames.Core.Models;
using SamuraiGames.Core.Types;
using SamuraiGames.QuickSlice.Events;
using SamuraiGames.QuickSlice.Game;
using SamuraiGames.QuickSlice.Game.Rounds;
using SamuraiGames.QuickSlice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.QuickSlice
{
    /// <summary>
    /// The context for an instance of the Quick Slice game.
    /// This doesn't control much for gameplay logic or rules, but rather it
    /// wires up all dependencies and initializes/shuts down all other systems.
    /// </summary>
    public sealed class QuickSliceGame : BaseContext
    {
        // Singleton setup, shouldn't need to modify much.
        public static QuickSliceGame instance { get { return _instance; } }
        private static readonly QuickSliceGame _instance = new QuickSliceGame();
        private QuickSliceGame() { }
        

        // Bindanble Interfaces
        // Interfaces for the client to bind, might move to a generic container class to explicitly show what should be bound.

        public IGameEngine gameEngine;

        // /Bindable Interfaces

        public GameModel gameModel { get; internal set; }

        public RoundModel roundModel { get { return gameEngine.currentRoundModel; } }

        public EngineEvents engineEvents { get { return _engineEvents; } }
        private EngineEvents _engineEvents;

        public ClientEvents clientEvents { get { return _clientEvents; } }
        private ClientEvents _clientEvents;

        private IDispatcher _engineDispatcher;
        private IDispatcher _clientDispatcher;        

        public override void Init()
        {
            _engineDispatcher = new EngineDispatcher();
            _engineEvents = new EngineEvents(_engineDispatcher);

            _clientDispatcher = new ClientDispatcher();
            _clientEvents = new ClientEvents(_clientDispatcher);

            LoadGameModel();

            gameEngine.Init(engineEvents, gameModel);            
        }

        public override void Start()
        {
            gameEngine.Start();
        }

        public override void Shutdown()
        {
            gameEngine.Shutdown();
        }

        override protected void LoadGameModel()
        {
            MinigameSetupModel setupModel;
            if (GameContext.instance.minigameSetupMap != null &&
                GameContext.instance.minigameSetupMap.ContainsKey(GameTypes.QUICK_SLICE))
            {
                setupModel = GameContext.instance.minigameSetupMap[GameTypes.QUICK_SLICE];
            }
            else
            {
                setupModel = GetDefaultSetupModel();
            }

            gameModel = new GameModel();
            gameModel.players = GetPlayers(setupModel);
            gameModel.numRounds = setupModel.numRounds;
            gameModel.rounds = new List<IRoundModel>();
        }

        // This might just stay for testing, as players would be chosen prior to this game.
        // AI and AI difficulty would be chosen ahead of time too.
        private List<IPlayer> GetPlayers(MinigameSetupModel setupModel)
        {
            List<IPlayer> players = new List<IPlayer>();
            players.Add(new HumanPlayer() { id = 0, playerName = "Player1" });
            players.Add(new AIPlayer() { id = 1, playerName = "AI", difficultyType = setupModel.difficulty });

            return players;
        }
        
        private MinigameSetupModel GetDefaultSetupModel()
        {
            MinigameSetupModel defaultSetupModel = new MinigameSetupModel()
            {
                numRounds = 3,
                maxRounds = 9,
                difficulty = DifficultyTypes.EASY
            };

            return defaultSetupModel;
        }        
    }
}
