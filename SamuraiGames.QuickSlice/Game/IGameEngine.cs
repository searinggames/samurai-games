﻿using SamuraiGames.Core.Events;
using SamuraiGames.QuickSlice.Events;
using SamuraiGames.QuickSlice.Game.Rounds;
using SamuraiGames.QuickSlice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.QuickSlice.Game
{
    public interface IGameEngine
    {
        GameModel gameModel { get; set; }

        RoundModel currentRoundModel { get; set; }

        void Init(EngineEvents engineEvents, GameModel gameModel);

        void Start();

        void Shutdown();        
    }
}
