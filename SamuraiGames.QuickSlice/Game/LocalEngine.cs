﻿using SamuraiGames.QuickSlice.Events;
using SamuraiGames.QuickSlice.Game.Rounds;
using SamuraiGames.QuickSlice.Models;
using System.Diagnostics;
using System;
using System.Collections.Generic;
using SamuraiGames.QuickSlice.Events.EngineEventArgs;
using SamuraiGames.QuickSlice.Events.ClientEventArgs;

namespace SamuraiGames.QuickSlice.Game
{
    /// <summary>
    /// Handles most game logic for local games.
    /// Typically catches most events from the client and typical
    /// dispatches most engine based events.
    /// </summary>
    public class LocalEngine : IGameEngine
    {
        public GameModel gameModel { get; set; }

        public RoundModel currentRoundModel { get { return _round.currentRound; } set { } }        

        internal EngineEvents _engineEvents;

        private Round _round;

        public void Init(EngineEvents engineEvents, GameModel gameModel)
        {
            _engineEvents = engineEvents;    
            this.gameModel = gameModel;

            _round = new Round(this.gameModel, _engineEvents);
        }

        public void Start()
        {
            ClientEvents.RoundIntroComplete += OnRoundIntroComplete;
            ClientEvents.PlayerFault += OnPlayerFault;
            ClientEvents.PlayerScored += OnPlayerScored;
            ClientEvents.PlayAgainSelected += OnPlayAgainSelected;

            NewGame(true);
        }        

        public void Shutdown()
        {
            ClientEvents.RoundIntroComplete -= OnRoundIntroComplete;
            ClientEvents.PlayerFault -= OnPlayerFault;
            ClientEvents.PlayerScored -= OnPlayerScored;
            ClientEvents.PlayAgainSelected -= OnPlayAgainSelected;
        }

        private void NewGame(bool firstPlay = false)
        {
            _round.Reset();

            if (firstPlay)
            {
                _engineEvents.OnMinigameReady(new MinigameReadyArgs() { quickSliceGameModel = gameModel });
            }
            else
            {
                _engineEvents.OnNewGameReady();
            }
            
        }

        private void OnRoundIntroComplete()
        {
            _round.StartNewRound();
        }

        private void OnPlayerScored(PlayerScoredArgs args)
        {
            // Get winning player model.
            IPlayer winningPlayer = GetPlayerModel(args.playerId);
            IPlayer losingPlayer = GetOpposingPlayerModel(args.playerId);
            _round.ResolveRound(winningPlayer, losingPlayer);
        }

        private void OnPlayerFault(PlayerFaultArgs args)
        {
            // Get winning player model (not the player at fault).
            IPlayer winningPlayer = GetOpposingPlayerModel(args.playerId);
            IPlayer losingPlayer = GetPlayerModel(args.playerId);
            _round.ResolveRound(winningPlayer, losingPlayer, true);
        }

        private void OnPlayAgainSelected()
        {
            NewGame();
        }

        private IPlayer GetPlayerModel(int playerId)
        {
            IPlayer selectedPlayer = null;
            foreach (IPlayer player in gameModel.players)
            {
                if (player.id == playerId)
                {
                    selectedPlayer = player;
                }
            }

            return selectedPlayer;
        }

        private IPlayer GetOpposingPlayerModel(int playerId)
        {
            IPlayer selectedPlayer = null;
            foreach (IPlayer player in gameModel.players)
            {
                if (player.id != playerId)
                {
                    selectedPlayer = player;
                }
            }

            return selectedPlayer;
        }        
    }
}
