﻿using SamuraiGames.QuickSlice.Events;
using SamuraiGames.QuickSlice.Events.EngineEventArgs;
using SamuraiGames.QuickSlice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.QuickSlice.Game.Rounds
{
    public class Round
    {        
        public RoundModel currentRound;

        private GameModel _gameModel;
        private EngineEvents _engineEvents;        

        private int _roundCounter;        

        public Round(GameModel gameModel, EngineEvents engineEvents)
        {
            _gameModel = gameModel;
            _engineEvents = engineEvents;
            _roundCounter = 0;
        }        

        public void StartNewRound()
        {
            _roundCounter++;
            currentRound = new RoundModel();
            _engineEvents.OnRoundStart(new RoundStartArgs() { roundModel = currentRound });
        }     
        
        public void ResolveRound(IPlayer winningPlayer, IPlayer losingPlayer, bool isFault = false)
        {
            currentRound.winningPlayerId = winningPlayer.id;
            currentRound.losingPlayerId = losingPlayer.id;
            _gameModel.rounds.Add(currentRound);

            // Check to see if the game has been resolved or just the current round.
            int minimumRounds = (int)(_gameModel.numRounds * 0.5) + 1;
            if (GetPlayerWins(winningPlayer.id) >= minimumRounds)
            {
                _engineEvents.OnGameResolved(new GameResolvedArgs() { winningPlayer = winningPlayer, isFault = isFault });
            }
            else
            {
                _engineEvents.OnRoundResolved(new RoundResolvedArgs() { winningPlayer = winningPlayer, isFault = isFault });
            }            
        }           

        public void Reset()
        {
            _roundCounter = 0;

            // TODO: Possibly store these rounds or the results for later record keeping.
            _gameModel.rounds = new List<IRoundModel>();
        }

        private int GetPlayerWins(int playerId)
        {
            int wins = 0;

            foreach(RoundModel round in _gameModel.rounds)
            {
                if (round.winningPlayerId == playerId)
                {
                    wins++;
                }
            }

            return wins;
        }                        
    }
}
