﻿using SamuraiGames.Core.Events;
using SamuraiGames.QuickSlice.Events.ClientEventArgs;
using System;

namespace SamuraiGames.QuickSlice.Events
{
    /// <summary>
    /// Quick Slice events dispatched by the client to the engine.
    /// Actions in this class are designed to be listened to by other engine
    /// classes and not the client.
    /// </summary>
    public class ClientEvents : EventsContainer
    {
        public ClientEvents(IDispatcher dispatcher) : base(dispatcher)
        {
        }

        /// <summary>
        /// Any UI or gameplay intro animations have finished on the client and
        /// a new round can begin.
        /// </summary>
        internal static event Action RoundIntroComplete;
        public void OnRoundIntroComplete() { _dispatcher.Dispatch(RoundIntroComplete); }

        /// <summary>
        /// The client has determined a player has faulted their attack.
        /// </summary>
        internal static event Action<PlayerFaultArgs> PlayerFault;
        public void OnPlayerFault(PlayerFaultArgs args) { _dispatcher.Dispatch(PlayerFault, args); }

        /// <summary>
        /// The client has determined a player has scored a succesful strike.
        /// </summary>
        internal static event Action<PlayerScoredArgs> PlayerScored;
        public void OnPlayerScored(PlayerScoredArgs args) { _dispatcher.Dispatch(PlayerScored, args); }

        /// <summary>
        /// An entire game has finished, and the client is wanting to play a new game
        /// over again.
        /// </summary>
        internal static event Action PlayAgainSelected;
        public void OnPlayAgainSelected() { _dispatcher.Dispatch(PlayAgainSelected); }
        
    }
}
