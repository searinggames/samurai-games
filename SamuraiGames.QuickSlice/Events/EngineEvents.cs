﻿using SamuraiGames.Core.Events;
using SamuraiGames.QuickSlice.Events.EngineEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.QuickSlice.Events
{
    /// <summary>
    /// Quick Slice events dispatched by the engine to the client.
    /// Actions in this class are designed to be listened to by the client
    /// and not by other engine classes.
    /// </summary>
    public class EngineEvents : EventsContainer
    {
        public EngineEvents(IDispatcher dispatcher) : base(dispatcher)
        {
        }

        /// <summary>
        /// The game's initialization is complete.
        /// </summary>
        public event Action<MinigameReadyArgs> MinigameReady;
        internal void OnMinigameReady(MinigameReadyArgs args) { _dispatcher.Dispatch(MinigameReady, args); }

        /// <summary>
        /// A game has been resolved and a new one is ready to start.
        /// </summary>
        public event Action NewGameReady;
        internal void OnNewGameReady() { _dispatcher.Dispatch(NewGameReady); }

        /// <summary>
        /// The game round has now started and the client can begin the gameplay.
        /// </summary>
        public event Action<RoundStartArgs> RoundStart;
        internal void OnRoundStart(RoundStartArgs args) { _dispatcher.Dispatch(RoundStart, args); }

        /// <summary>
        /// The current round has been resolved, with a winner determined.
        /// </summary>
        public event Action<RoundResolvedArgs> RoundResolved;
        internal void OnRoundResolved(RoundResolvedArgs args) { _dispatcher.Dispatch(RoundResolved, args); }

        /// <summary>
        /// All rounds for the game have been played and the game is finished.
        /// </summary>
        public event Action<GameResolvedArgs> GameResolved;
        internal void OnGameResolved(GameResolvedArgs args) { _dispatcher.Dispatch(GameResolved, args); }
    }
}
