﻿using SamuraiGames.QuickSlice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.QuickSlice.Events.EngineEventArgs
{
    public class GameResolvedArgs
    {
        public IPlayer winningPlayer;

        public bool isFault;
    }
}
