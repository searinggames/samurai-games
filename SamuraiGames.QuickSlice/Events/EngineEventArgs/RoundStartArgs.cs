﻿using SamuraiGames.QuickSlice.Game.Rounds;
using SamuraiGames.QuickSlice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.QuickSlice.Events.EngineEventArgs
{
    public class RoundStartArgs
    {
        public IRoundModel roundModel;
    }
}
