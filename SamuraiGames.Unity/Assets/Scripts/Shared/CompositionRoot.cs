﻿using SamuraiGames.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SamuraiGames.Unity.Shared
{
    public abstract class CompositionRoot : MonoBehaviour
    {
        public RootController rootController;

        protected virtual void Awake()
        {
            SetBindings();
            InitContext();
        }

        // Use this for initialization
        protected virtual void Start()
        {
            rootController.PostBindSetup();
        }

        protected virtual void OnDestroy()
        {
            ShutdownContext();
        }

        /// <summary>
        /// Set any bindings within the context's container.
        /// </summary>
        protected abstract void SetBindings();

        /// <summary>
        /// Initialize the context once bindings are complete.
        /// </summary>
        protected abstract void InitContext();

        /// <summary>
        /// Call any shutdown logic on the context.
        /// </summary>
        protected abstract void ShutdownContext();
    }
}