﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SamuraiGames.Unity.Shared
{
    public abstract class RootController : MonoBehaviour
    {
        /// <summary>
        /// Controller setup to be ran once all bindings and initialization are complete.
        /// Should never be called from Awake() as the RootController might not have hit its Awake() yet.
        /// </summary>
        public abstract void PostBindSetup();

    }
}
