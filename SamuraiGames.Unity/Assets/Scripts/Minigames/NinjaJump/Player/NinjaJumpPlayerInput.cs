﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace SamuraiGames.Unity.NinjaJump
{
    public class NinjaJumpPlayerInput : NinjaJumpBaseInput
    {
        private Player _playerInput;        

        // Use this for initialization
        void Start()
        {
            _playerInput = ReInput.players.GetPlayer(this.playerInputId);
            _playerInput.controllers.maps.SetAllMapsEnabled(false);
        }

        public override void Activate()
        {
            _playerInput.controllers.maps.SetMapsEnabled(true, "Gameplay");
        }

        public override void Deactivate()
        {
            _playerInput.controllers.maps.SetMapsEnabled(false, "Gameplay");
        }

        // Update is called once per frame
        void Update()
        {
            ProcessInput();
        }

        private void ProcessInput()
        {
            if (_playerInput.GetButtonDown("Action2"))
            {
                this.TryJump();
            }
            else if (_playerInput.GetButtonDown("Action1"))
            {
                this.TryAttack();
            }
        }
    }
}