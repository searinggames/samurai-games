﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace SamuraiGames.Unity.NinjaJump
{
    public class NinjaJumpPlayerJump : MonoBehaviour
    {
        // For testing, can be removed and possibly set explicitly in a factory function.
        public Transform apex;

        public float timeToMaxHeight;

        public bool isAirborne { get; private set; }

        private Vector2 _startingPosition;

        private float _gravity;

        private float _initialVelocityY;

        private float _initialVelocityX;

        private float _landPosX;

        private float _airTime = 0f;

        private Coroutine _jumpCoroutine = null;

        // Use this for initialization
        void Start()
        {
            _startingPosition = this.transform.position;
            float maxHeight = Mathf.Abs(apex.position.y - this.transform.position.y);
            float maxDistance = Mathf.Abs(apex.position.x - this.transform.position.x);
            _gravity = GetGravityConstant(maxHeight, timeToMaxHeight);
            _initialVelocityY = GetInitialVelocityY(maxHeight, timeToMaxHeight);
            _initialVelocityX = GetInitialVelocityX(maxDistance, timeToMaxHeight);

            _landPosX = GetLandingPosX();
            
            
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void Reset()
        {
            isAirborne = false;
            _airTime = 0f;
        }

        public void Jump()
        {
            isAirborne = true;
            _airTime = 0f;
            _jumpCoroutine = this.StartCoroutine(JumpLerp(timeToMaxHeight, _initialVelocityX, _initialVelocityY, _gravity));
        }

        public void KillJump()
        {            
            if (isAirborne)
            {                
                // Make sure the to only kill the jump on ascent.
                if (_airTime < timeToMaxHeight)
                {                    
                    float maxHeight = Mathf.Abs(this.transform.position.y - _startingPosition.y);
                    float maxDistance = Mathf.Abs(this.transform.position.x - _startingPosition.x);
                    float gravity = GetGravityConstant(maxHeight, _airTime);
                    float velocityX = GetInitialVelocityX(maxDistance, _airTime);
                    float velocityY = GetInitialVelocityY(maxHeight, _airTime);

                    Debug.Log("Kill jump.");
                    this.StopCoroutine(_jumpCoroutine);
                    _jumpCoroutine = this.StartCoroutine(JumpLerp(_airTime, velocityX, velocityY, gravity));
                }
            }
        }

        private IEnumerator JumpLerp(float timeToApex, float velocityX, float velocityY, float gravity)
        {
            Vector2 startPosition = _startingPosition;

            while (_airTime < (timeToApex * 2))
            {
                _airTime += Time.deltaTime;

                float posX = GetMotionX(_airTime, startPosition, velocityX);
                float posY = GetMotionY(_airTime, startPosition, velocityY, gravity);
                Vector2 newPos = new Vector2(posX, posY);
                this.transform.position = newPos;

                yield return null;
            }
        }

        private float GetLandingPosX()
        {
            float halfDistance = apex.position.x - this.transform.position.x;
            float direction = Mathf.Sign(halfDistance);
            float distance = halfDistance * 2f;
            float posX = this.transform.position.x + (direction * distance);

            return posX;
        }

        private float GetGravityConstant(float height, float timeToHeight)
        {
            float gravity = 0;
            gravity = (-2 * height) / (Mathf.Pow(timeToHeight, 2));

            return gravity;
        }

        private float GetInitialVelocityY(float height, float timeToHeight)
        {
            float velocity = 0;
            velocity = (2 * height) / timeToHeight;

            return velocity;
        }

        private float GetInitialVelocityX(float distance, float timeToHeight)
        {
            float velocity = 0;
            velocity = (2 * distance) / (timeToHeight * 2f);

            return velocity;
        }

        private float GetMotionY(float time, Vector2 pos, float velocityY, float gravity)
        {
            float val = (0.5f * gravity * Mathf.Pow(time, 2)) + (velocityY * time) + pos.y;
                        
            return val;
        }

        private float GetMotionX(float time, Vector2 pos, float velocityX)
        {
            float val = (velocityX * time) + pos.x;

            return val;
        }
    }
}