﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SamuraiGames.Unity.NinjaJump
{
    public abstract class NinjaJumpBaseInput : MonoBehaviour
    {
        public event Action OnJumpPressed;

        public event Action OnAttackPressed;

        public int playerInputId;

        public abstract void Activate();

        public abstract void Deactivate();

        public void Reset()
        {

        }

        protected void TryJump()
        {
            if (OnJumpPressed != null)
            {
                OnJumpPressed();
            }
        }

        protected void TryAttack()
        {
            if (OnAttackPressed != null)
            {
                OnAttackPressed();
            }
        }
    }
}