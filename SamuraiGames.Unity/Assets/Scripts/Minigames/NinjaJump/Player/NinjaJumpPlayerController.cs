﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SamuraiGames.Unity.NinjaJump
{
    public class NinjaJumpPlayerController : MonoBehaviour
    {
        public int playerId;

        private NinjaJumpBaseInput _playerInput;
        private Animator _animator;
        private NinjaJumpPlayerJump _playerJump;
        private Vector3 _startPositon;

        void Awake()
        {
            _playerInput = this.GetComponent<NinjaJumpBaseInput>();
            _animator = this.GetComponentInChildren<Animator>();
            _playerJump = this.GetComponent<NinjaJumpPlayerJump>();
        }

        // Use this for initialization
        void Start()
        {
            _startPositon = this.transform.position;

            // Calling here for testing.
            _playerInput.Activate();
            _playerJump.Reset();
        }

        void OnEnable()
        {
            _playerInput.OnJumpPressed += OnJumpPressed;
            _playerInput.OnAttackPressed += OnAttackPressed;
        }
        
        void OnDisable()
        {
            _playerInput.OnJumpPressed -= OnJumpPressed;
            _playerInput.OnAttackPressed -= OnAttackPressed;
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnJumpPressed()
        {
            if (!_playerJump.isAirborne)
            {
                _playerJump.Jump();
            }
            else
            {
                // For testing, reset the player.
            }
        }

        private void OnAttackPressed()
        {
            Debug.Log("Attack pressed.");

            if (_playerJump.isAirborne)
            {
                _playerJump.KillJump();
            }
        }
    }
}