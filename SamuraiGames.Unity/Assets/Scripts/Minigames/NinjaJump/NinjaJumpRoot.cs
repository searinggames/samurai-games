﻿using SamuraiGames.Unity.Shared;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SamuraiGames.NinjaJump;

namespace SamuraiGames.Unity.NinjaJump
{
    public class NinjaJumpRoot : CompositionRoot
    {
        protected override void Awake()
        {
            base.Awake();
        }

        // Use this for initialization
        protected override void Start()
        {
            base.Start();
            NinjaJumpGame.instance.Start();
        }        

        protected override void SetBindings()
        {
            // Bind anything in the context to concrete instances.
        }

        protected override void InitContext()
        {
            NinjaJumpGame.instance.Init();
        }

        protected override void ShutdownContext()
        {
            NinjaJumpGame.instance.Shutdown();
        }        
    }
}