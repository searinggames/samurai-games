﻿using SamuraiGames.Core.Events;
using SamuraiGames.QuickSlice;
using SamuraiGames.QuickSlice.Events;
using SamuraiGames.QuickSlice.Events.ClientEventArgs;
using SamuraiGames.QuickSlice.Events.EngineEventArgs;
using SamuraiGames.QuickSlice.Models;
using SamuraiGames.Unity.Shared;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
using SamuraiGames.Unity.UI;
using UnityEngine.SceneManagement;

namespace SamuraiGames.Unity.QuickSlice
{
    public class QuickSliceController : RootController
    {
        public static event Action OnNewRoundReset;
        public static event Action OnPlayerFaulted;
        
        private QuickSliceUIController _uiController;
        private QuickSliceTimer _timer;
        private QuickSlicePlayerFactory _playerFactory;

        // The id of the first player to successfully attack.
        private int _firstAttackPlayerId = -1;

        void Awake()
        {
            _uiController = GameObject.Find("GameUI").GetComponent<QuickSliceUIController>();
            _timer = GameObject.Find("Timer").GetComponent<QuickSliceTimer>();
            _playerFactory = GameObject.Find("Players").GetComponent<QuickSlicePlayerFactory>();
        }

        // Use this for initialization
        void Start()
        {

        }

        void OnEnable()
        {
            QuickSliceGame.instance.engineEvents.MinigameReady += OnMinigameReady;
            QuickSliceGame.instance.engineEvents.RoundStart += OnRoundStart;
            QuickSliceGame.instance.engineEvents.RoundResolved += OnRoundResolved;
            QuickSliceGame.instance.engineEvents.GameResolved += OnGameResolved;
            QuickSliceGame.instance.engineEvents.NewGameReady += OnNewGameReady;
            QuickSlicePlayerController.OnPlayerAttacked += OnPlayerAttacked;
            EndRoundReturnItem.OnReturnPressed += OnReturnPressed;
            EndRoundPlayAgainItem.OnPlayAgainPressed += OnPlayAgainPressed;
        }        

        void OnDisable()
        {
            QuickSliceGame.instance.engineEvents.MinigameReady -= OnMinigameReady;
            QuickSliceGame.instance.engineEvents.RoundStart -= OnRoundStart;
            QuickSliceGame.instance.engineEvents.RoundResolved -= OnRoundResolved;
            QuickSliceGame.instance.engineEvents.GameResolved -= OnGameResolved;
            QuickSliceGame.instance.engineEvents.NewGameReady -= OnNewGameReady;
            QuickSlicePlayerController.OnPlayerAttacked -= OnPlayerAttacked;            
            EndRoundReturnItem.OnReturnPressed -= OnReturnPressed;
            EndRoundPlayAgainItem.OnPlayAgainPressed -= OnPlayAgainPressed;
        }

        public override void PostBindSetup()
        {

        }

        private void OnMinigameReady(MinigameReadyArgs args)
        {
            _playerFactory.AddPlayers(args.quickSliceGameModel.players);            

            // When the minigame is ready, we can start our first round intro.
            _uiController.StartRoundIntro();

        }

        private void OnNewGameReady()
        {
            _uiController.StartRoundIntro();

            if (OnNewRoundReset != null)
            {
                OnNewRoundReset();
            }
        }

        private void OnRoundStart(RoundStartArgs args)
        {
            _firstAttackPlayerId = -1;
        }

        private void OnPlayerAttacked(int playerId)
        {
            if (_timer.currentTime > 0)
            {
                // Send out a local event to any other unity objects that a player faulted.
                if (OnPlayerFaulted != null)
                {
                    OnPlayerFaulted();
                }

                QuickSliceGame.instance.clientEvents.OnPlayerFault(new PlayerFaultArgs() { playerId = playerId });
            }
            else
            {
                if (_firstAttackPlayerId < 0)
                {
                    _firstAttackPlayerId = playerId;
                    QuickSliceGame.instance.clientEvents.OnPlayerScored(new PlayerScoredArgs() { playerId = playerId });
                }
            }
        }

        private void OnRoundResolved(RoundResolvedArgs args)
        {
            Debug.Log(args.winningPlayer.playerName + " wins the round!");
            _timer.Cancel();
            StartNextRoundWithDelay();
        }        

        private void StartNextRoundWithDelay()
        {
            DOVirtual.DelayedCall(3f, () =>
            {
                _uiController.StartRoundIntro();
                if (OnNewRoundReset != null)
                {
                    OnNewRoundReset();
                }
            });
        }

        private void OnGameResolved(GameResolvedArgs args)
        {
            Debug.Log(args.winningPlayer.playerName + " wins the game!");
            _timer.Cancel();
            _uiController.ShowEndRoundPopUp(args.winningPlayer.playerName);
        }

        private void OnPlayAgainPressed()
        {
            QuickSliceGame.instance.clientEvents.OnPlayAgainSelected();
        }

        private void OnReturnPressed()
        {
            SceneManager.LoadScene("TestMenu");
        }
    }
}