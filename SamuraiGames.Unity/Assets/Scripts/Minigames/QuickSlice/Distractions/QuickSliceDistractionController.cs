﻿using SamuraiGames.QuickSlice;
using SamuraiGames.QuickSlice.Events.EngineEventArgs;
using SamuraiGames.Unity.QuickSlice;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SamuraiGames.Unity.QuickSlice
{
    public class QuickSliceDistractionController : MonoBehaviour
    {
        public QuickSliceRoundSettings roundSettings;

        private int _numRoundDistractions;

        private int _roundDistractionsCounter;

        private float _roundTime;

        // Use this for initialization
        void Start()
        {

        }

        void OnEnable()
        {
            QuickSliceGame.instance.engineEvents.RoundStart += OnRoundStart;
            QuickSliceGame.instance.engineEvents.RoundResolved += OnRoundResolved;
            QuickSliceGame.instance.engineEvents.GameResolved += OnGameResolved;
            QuickSliceTimer.OnRoundTimerSet += OnRoundTimerSet;
        }        

        void OnDisable()
        {
            QuickSliceGame.instance.engineEvents.RoundStart -= OnRoundStart;
            QuickSliceGame.instance.engineEvents.RoundResolved -= OnRoundResolved;
            QuickSliceGame.instance.engineEvents.GameResolved -= OnGameResolved;
            QuickSliceTimer.OnRoundTimerSet -= OnRoundTimerSet;
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        private void OnRoundStart(RoundStartArgs args)
        {            
            
        }

        private void OnRoundResolved(RoundResolvedArgs args)
        {
            Reset();
        }

        private void OnGameResolved(GameResolvedArgs args)
        {
            Reset();
        }

        private void OnRoundTimerSet(float roundTime)
        {
            _roundTime = roundTime;
            _roundDistractionsCounter = 0;
            _numRoundDistractions = GetDistractionCountForRound(_roundTime);
            Debug.Log("Distractions for round: " + _numRoundDistractions);
            this.StartCoroutine(WaitForNextDistraction(_numRoundDistractions, _roundTime));
        }

        private IEnumerator WaitForNextDistraction(int numDistractions, float roundTime)
        {
            float distractionInterval = GetDistractionInterval(numDistractions, _roundTime);
            float distractionTimer = 0f;

            while (distractionTimer < distractionInterval)
            {
                distractionTimer += Time.deltaTime;
                yield return null;
            }

            StartDistraction();
        }

        private int GetDistractionCountForRound(float roundTime)
        {
            int distractionsCount = Random.Range(1, roundSettings.maxDistractions);

            // If we have more distractions than half the seconds in the round, we need to reduce it.
            if ((distractionsCount + 1) > (_roundTime * 0.5))
            {
                distractionsCount = Mathf.Max(0, (int)(_roundTime * 0.5f) - 1);
            }

            return distractionsCount;
        }

        private float GetDistractionInterval(int numDistractions, float roundTime)
        {
            // Increment the number of distractions by one because we want the interval
            // to make sure all distractions happen within the time frame, not just at the end.
            float interval = roundTime / (numDistractions + 1);

            return interval;
        }

        private void StartDistraction()
        {
            _roundDistractionsCounter++;
            int randomNum = Random.Range(0, 3);

            if (randomNum == 0)
            {
                Debug.Log("Start visual distraction.");
            }
            else if (randomNum == 1)
            {
                Debug.Log("Start audio distraction.");
            }
            else
            {
                Debug.Log("Don't start any distractions.");
            }

            if (_roundDistractionsCounter < _numRoundDistractions)
            {
                this.StartCoroutine(WaitForNextDistraction(_numRoundDistractions, _roundTime));
            }
        }

        private void Reset()
        {
            //this.StopCoroutine("WaitForNextDistraction");
            this.StopAllCoroutines();
        }
    }
}