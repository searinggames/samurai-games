﻿using SamuraiGames.QuickSlice;
using SamuraiGames.QuickSlice.Models;
using SamuraiGames.Unity.Shared;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SamuraiGames.QuickSlice.Game;

namespace SamuraiGames.Unity.QuickSlice
{
    public class QuickSliceRoot : CompositionRoot
    {
        protected override void Awake()
        {
            base.Awake();
        }

        // Use this for initialization
        protected override void Start()
        {
            base.Start();
            QuickSliceGame.instance.Start();
        }

        protected override void SetBindings()
        {
            // Bind anything in the context to concrete instances.
            QuickSliceGame.instance.gameEngine = new LocalEngine();
        }

        protected override void InitContext()
        {
            QuickSliceGame.instance.Init();
        }

        protected override void ShutdownContext()
        {
            QuickSliceGame.instance.Shutdown();
        }
    }
}