﻿using SamuraiGames.QuickSlice;
using SamuraiGames.QuickSlice.Events.EngineEventArgs;
using SamuraiGames.Unity.QuickSlice;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SamuraiGames.Unity.QuickSlice
{
    public class QuickSliceTimer : MonoBehaviour
    {
        public delegate void RoundTimerSet(float roundTime);
        public static event RoundTimerSet OnRoundTimerSet;

        public static event Action OnTimerExpired;

        public QuickSliceRoundSettings roundSettings;

        private float _currentTime;
        public float currentTime { get { return _currentTime; } }

        // Use this for initialization
        void Start()
        {

        }

        void OnEnable()
        {
            QuickSliceGame.instance.engineEvents.RoundStart += OnRoundStart;
        }

        void OnDisable()
        {
            QuickSliceGame.instance.engineEvents.RoundStart -= OnRoundStart;
        }

        // Update is called once per frame
        void Update()
        {
            if (_currentTime > 0)
            {
                _currentTime -= Time.deltaTime;
                if (_currentTime <= 0)
                {
                    Debug.Log("Fight!");
                    _currentTime = 0;
                    if (OnTimerExpired != null)
                    {
                        OnTimerExpired();
                    }
                }
            }
        }

        public void Cancel()
        {
            _currentTime = 0;
        }

        private void OnRoundStart(RoundStartArgs args)
        {
            _currentTime = GetRandomRoundLength();
            if (OnRoundTimerSet != null)
            {
                OnRoundTimerSet(_currentTime);
            }

            Debug.Log("Round time: " + _currentTime);
        }

        private float GetRandomRoundLength()
        {
            float randomRoundLength = UnityEngine.Random.Range(roundSettings.roundTimeMin, roundSettings.roundTimeMax);

            return randomRoundLength;
        }
    }
}