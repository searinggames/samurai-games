﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using SamuraiGames.QuickSlice;
using System;

namespace SamuraiGames.Unity.QuickSlice
{
    public class GetReadyText : MonoBehaviour
    {
        public static event Action OnIntroAnimationComplete;

        // Use this for initialization
        void Start()
        {
            //StartIntroAnimation();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void StartIntroAnimation()
        {
            this.transform.localScale = new Vector3(0, 1, 1);

            Sequence introSequence = DOTween.Sequence();
            introSequence.Append(this.transform.DOScaleX(1f, 1f).SetEase(Ease.InExpo));
            introSequence.AppendInterval(1f);
            introSequence.Append(this.transform.DOScaleY(0f, 1f));
            introSequence.AppendCallback(IntroAnimationFinished);
        }

        private void IntroAnimationFinished()
        {
            if (OnIntroAnimationComplete != null)
            {
                OnIntroAnimationComplete();
            }
        }
    }
}