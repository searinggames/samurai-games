﻿using Rewired;
using SamuraiGames.QuickSlice;
using SamuraiGames.Unity.QuickSlice;
using SamuraiGames.Unity.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickSliceUIController : MonoBehaviour
{
    private GetReadyText _readyText;
    private EndRoundPanel _endRoundPanel;

    void Awake()
    {
        _readyText = this.GetComponentInChildren<GetReadyText>();
        _endRoundPanel = this.GetComponentInChildren<EndRoundPanel>();
    }
    
    // Use this for initialization
    void Start()
    {
        _endRoundPanel.Hide();
    }

    void OnEnable()
    {
        GetReadyText.OnIntroAnimationComplete += OnIntroAnimationComplete;
    }    

    void OnDisable()
    {
        GetReadyText.OnIntroAnimationComplete += OnIntroAnimationComplete;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void StartRoundIntro()
    {
        _endRoundPanel.Hide();
        _readyText.StartIntroAnimation();
    }

    public void ShowEndRoundPopUp(string winningPlayerName)
    {
        _endRoundPanel.Hide();
        _endRoundPanel.Show(winningPlayerName);
    }

    private void OnIntroAnimationComplete()
    {
        QuickSliceGame.instance.clientEvents.OnRoundIntroComplete();
    }    
}
