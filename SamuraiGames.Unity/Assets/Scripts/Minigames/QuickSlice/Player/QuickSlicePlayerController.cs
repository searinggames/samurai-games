﻿using SamuraiGames.QuickSlice;
using SamuraiGames.QuickSlice.Events.EngineEventArgs;
using UnityEngine;
using DG.Tweening;

namespace SamuraiGames.Unity.QuickSlice
{
    public class QuickSlicePlayerController : MonoBehaviour
    {
        public delegate void PlayerAttacked(int playerId);
        public static event PlayerAttacked OnPlayerAttacked;

        public int playerId;

        private QuickSliceBaseInput _playerInput;

        private Animator _animator;

        private Vector3 _startPosition;

        void Awake()
        {
            _playerInput = this.GetComponent<QuickSliceBaseInput>();
            _animator = this.GetComponentInChildren<Animator>();
        }

        // Use this for initialization
        void Start()
        {
            _startPosition = this.transform.position;
        }

        void OnEnable()
        {
            QuickSliceGame.instance.engineEvents.RoundStart += OnRoundStart;
            QuickSliceGame.instance.engineEvents.RoundResolved += OnRoundResolved;
            QuickSliceGame.instance.engineEvents.GameResolved += OnGameResolved;
            _playerInput.OnDrawPressed += OnDrawPressed;
            _playerInput.OnSlicePressed += OnSlicePressed;
            QuickSliceController.OnNewRoundReset += OnNewRoundReset;
        }        

        void OnDisable()
        {
            QuickSliceGame.instance.engineEvents.RoundStart -= OnRoundStart;
            QuickSliceGame.instance.engineEvents.RoundResolved -= OnRoundResolved;
            QuickSliceGame.instance.engineEvents.GameResolved -= OnGameResolved;
            _playerInput.OnDrawPressed -= OnDrawPressed;
            _playerInput.OnSlicePressed -= OnSlicePressed;
            QuickSliceController.OnNewRoundReset -= OnNewRoundReset;
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnRoundStart(RoundStartArgs args)
        {
            _playerInput.Reset();
            _playerInput.Activate();            
        }

        private void OnDrawPressed()
        {
            Debug.Log("Draw!");
            _animator.SetTrigger("draw_trigger");
        }

        private void OnSlicePressed()
        {
            Debug.Log("Slice!");
            if (OnPlayerAttacked != null)
            {
                OnPlayerAttacked(playerId);
            }
        }

        private void OnRoundResolved(RoundResolvedArgs args)
        {
            ActionResolved(args.winningPlayer.id, args.isFault);
        }

        private void OnGameResolved(GameResolvedArgs args)
        {
            ActionResolved(args.winningPlayer.id, args.isFault);
        }

        private void ActionResolved(int winningPlayerId, bool isFault)
        {
            _playerInput.Deactivate();

            if (winningPlayerId == playerId)
            {
                PlayerWins(isFault);
            }
            else
            {
                PlayerLost();
            }
        }

        private void PlayerWins(bool isFault = false)
        {
            if (!isFault)
            {
                _animator.SetTrigger("attack_trigger");
                float newPosX = -this.transform.position.x;
                this.transform.DOMoveX(newPosX, 0.05f);
            }
        }

        private void PlayerLost()
        {
            DOVirtual.DelayedCall(0.1f, () => { _animator.SetTrigger("dead_trigger"); });
        }

        private void OnNewRoundReset()
        {
            this.transform.position = _startPosition;
            _animator.SetTrigger("idle_trigger");
        }
    }
}