﻿using SamuraiGames.QuickSlice;
using SamuraiGames.Unity.QuickSlice;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickSliceAIInput : QuickSliceBaseInput
{
    public QuickSliceAISettings aiSettings;

    private float _nextReactionTime;
    
    // Use this for initialization
    void Start () {
		
	}

    void OnEnable()
    {
        QuickSliceTimer.OnTimerExpired += OnTimerExpired;
    }    

    void OnDisable()
    {
        QuickSliceTimer.OnTimerExpired -= OnTimerExpired;
    }

    public override void Activate()
    {
        _nextReactionTime = GetNextReactionTime();
        Debug.Log("Reaction time: " + _nextReactionTime);
    }

    public override void Deactivate()
    {
        this.StopCoroutine("StartSliceReaction");
    }

    // Update is called once per frame
    void Update () {
		
	}

    private float GetNextReactionTime()
    {
        float reactionTime = UnityEngine.Random.Range(aiSettings.reactionTimeMin, aiSettings.reactionTimeMax);

        return reactionTime;
    }

    private void OnTimerExpired()
    {
        this.StartCoroutine("StartSliceReaction");
    }

    private IEnumerator StartSliceReaction()
    {
        float reactionTimer = 0f;

        while (reactionTimer < _nextReactionTime)
        {
            reactionTimer += Time.deltaTime;
            yield return null;
        }
        
        while (this._drawCooldownTimer > 0)
        {
            yield return null;
        }

        this.TrySlice();
    }
}
