﻿using SamuraiGames.Core.Types;
using SamuraiGames.QuickSlice.Models;
using SamuraiGames.Unity.QuickSlice;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickSlicePlayerFactory : MonoBehaviour
{
    public GameObject humanPlayerPrefab;

    public GameObject aiPlayerPrefab;

    public QuickSliceAISettings aiSettingsEasy;
    public QuickSliceAISettings aiSettingsNormal;
    public QuickSliceAISettings aiSettingsHard;

    public GameObject leftStartPosition;
    public GameObject rightStartPosition;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AddPlayers(List<IPlayer> players)
    {
        for (int i = 0; i < players.Count ; i++)
        {
            GameObject playerGO;

            if (players[i].isAI)
            {
                playerGO = GameObject.Instantiate(aiPlayerPrefab);

                AIPlayer aiPlayer = (AIPlayer)players[i];
                QuickSliceAISettings aiSettings = aiSettingsEasy;
                
                if (aiPlayer.difficultyType == DifficultyTypes.NORMAL)
                {
                    aiSettings = aiSettingsNormal;
                }
                else if (aiPlayer.difficultyType == DifficultyTypes.HARD)
                {
                    aiSettings = aiSettingsHard;
                }

                playerGO.GetComponent<QuickSliceAIInput>().aiSettings = aiSettings;
            }
            else
            {
                playerGO = GameObject.Instantiate(humanPlayerPrefab);                
            }

            // Set the player id set by the engine.
            QuickSlicePlayerController playerController = playerGO.GetComponent<QuickSlicePlayerController>();
            playerController.playerId = players[i].id;

            // Set the local player id used for any input. We assume the order of the players in the list
            // set by the engine, determines the player index for input.
            QuickSliceBaseInput playerInput = playerGO.GetComponent<QuickSliceBaseInput>();
            playerInput.playerInputId = i;

            playerGO.transform.parent = this.transform;

            if (i == 0)
            {
                playerGO.transform.position = leftStartPosition.transform.position;
            }
            else
            {
                playerGO.transform.position = rightStartPosition.transform.position;

                // Flip the sprite.
                playerGO.GetComponentInChildren<SpriteRenderer>().flipX = true;
            }
        }       
    }
}
