﻿using Rewired;
using System;

public class QuickSlicePlayerInput : QuickSliceBaseInput
{
    private Player _playerInput;

    void Awake()
    {
       
    }

    // Use this for initialization
    void Start()
    {
        _playerInput = ReInput.players.GetPlayer(this.playerInputId);
        _playerInput.controllers.maps.SetAllMapsEnabled(false);
    }

    public override void Activate()
    {
        _playerInput.controllers.maps.SetMapsEnabled(true, "Gameplay");
    }

    public override void Deactivate()
    {
        _playerInput.controllers.maps.SetMapsEnabled(false, "Gameplay");
    }

    // Update is called once per frame
    void Update()
    {
        ProcessInput();
    }    

    private void ProcessInput()
    {
        if (_playerInput.GetButtonDown("Action2"))
        {
            this.TryDraw();
        }
        else if (_playerInput.GetButtonDown("Action1"))
        {
            this.TrySlice();
        }
    }    
}
