﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class QuickSliceBaseInput : MonoBehaviour
{
    public event Action OnDrawPressed;

    public event Action OnSlicePressed;

    public float drawCooldown;

    public int playerInputId;

    protected float _drawCooldownTimer;

    public abstract void Activate();

    public abstract void Deactivate();

    public void Reset()
    {
        this.StopCoroutine("StartDrawCooldown");
        this._drawCooldownTimer = 0f;
    }

    protected void TryDraw()
    {
        if (OnDrawPressed != null && _drawCooldownTimer <= 0f)
        {
            this.StartCoroutine("StartDrawCooldown");
            OnDrawPressed();
        }
    }

    protected void TrySlice()
    {
        if (OnSlicePressed != null && _drawCooldownTimer <= 0f)
        {
            OnSlicePressed();
        }
    }

    private IEnumerator StartDrawCooldown()
    {
        _drawCooldownTimer = drawCooldown;
        while (_drawCooldownTimer > 0)
        {
            _drawCooldownTimer -= Time.deltaTime;
            yield return null;
        }

        _drawCooldownTimer = 0;
    }
}
