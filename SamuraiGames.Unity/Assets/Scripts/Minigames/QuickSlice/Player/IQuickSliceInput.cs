﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IQuickSliceInput
{
    event Action OnDrawPressed;

    event Action OnSlicePressed;

    int playerId { get; set; }

    float drawCooldown { get; set; }

    void SetEnabled(bool isEnabled);
}
