﻿using SamuraiGames.QuickSlice;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SamuraiGames.Unity.QuickSlice
{
    public class JudgeController : MonoBehaviour
    {
        private Animator _animator;
        private SpriteRenderer _spriteRenderer;

        void Awake()
        {
            _animator = this.GetComponent<Animator>();
            _spriteRenderer = this.GetComponent<SpriteRenderer>();
        }

        // Use this for initialization
        void Start()
        {
            _spriteRenderer.flipX = getRandomFlipX();
        }

        void OnEnable()
        {
            QuickSliceGame.instance.engineEvents.RoundStart += OnRoundStart;
            QuickSliceTimer.OnTimerExpired += OnTimerExpired;
            QuickSliceController.OnNewRoundReset += OnNewRoundReset;
            QuickSliceController.OnPlayerFaulted += OnPlayerFaulted;
        }        

        void OnDisable()
        {
            QuickSliceGame.instance.engineEvents.RoundStart -= OnRoundStart;
            QuickSliceTimer.OnTimerExpired -= OnTimerExpired;
            QuickSliceController.OnNewRoundReset -= OnNewRoundReset;
            QuickSliceController.OnPlayerFaulted -= OnPlayerFaulted;
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnRoundStart(SamuraiGames.QuickSlice.Events.EngineEventArgs.RoundStartArgs obj)
        {
            _spriteRenderer.flipX = getRandomFlipX();
            _animator.SetTrigger("ready1_trigger");
        }

        private void OnTimerExpired()
        {
            _animator.SetTrigger("fight_trigger");
        }

        private void OnNewRoundReset()
        {
            _animator.SetTrigger("idle_trigger");                        
        }

        private void OnPlayerFaulted()
        {
            // TODO: Trigger a fault specific animation.
            _animator.SetTrigger("idle_trigger");
        }

        private bool getRandomFlipX()
        {
            bool isFlippedX = false;
            if (Random.Range(0, 2) > 0)
            {
                isFlippedX = true;
            }

            return isFlippedX;
        }
    }
}