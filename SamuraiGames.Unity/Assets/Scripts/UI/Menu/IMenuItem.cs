﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SamuraiGames.Unity.UI
{
    public interface IMenuItem
    {
        void Highlight();

        void Unhighlight();

        void Press();
    }
}