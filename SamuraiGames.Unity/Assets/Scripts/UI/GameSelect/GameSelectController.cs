﻿using Rewired;
using SamuraiGames.Core.Game;
using SamuraiGames.Core.Types;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SamuraiGames.Unity.UI
{
    public class GameSelectController : MonoBehaviour
    {
        private int _playerId = 0;
        private Player _playerInput;
        private GameSelectPanel _gameSelectPanel;
        private GameSelectTransition _transition;

        void Awake()
        {
            _playerInput = ReInput.players.GetPlayer(_playerId);
            _gameSelectPanel = this.GetComponentInChildren<GameSelectPanel>();
            _transition = this.GetComponentInChildren<GameSelectTransition>();
        }

        // Use this for initialization
        void Start()
        {
            _playerInput.controllers.maps.SetAllMapsEnabled(false);
            _playerInput.controllers.maps.SetMapsEnabled(true, "Menu");
        }

        public void OnEnable()
        {
            _transition.OnTransitionComplete += OnTransitionComplete;
        }

        public void OnDisable()
        {
            _transition.OnTransitionComplete -= OnTransitionComplete;
        }

        // Update is called once per frame
        void Update()
        {
            ProcessInput();
        }

        private void ProcessInput()
        {
            if (_playerInput.GetButtonDown("Confirm"))
            {
                GoToGame();
            }
            else if (_playerInput.GetButtonDown("Cancel"))
            {

            }
        }

        private void GoToGame()
        {
            _playerInput.controllers.maps.SetAllMapsEnabled(false);

            GameTypes selectedGameType = _gameSelectPanel.GetSelectedGameType();
            _transition.StartTransition();

            // Set game model variables.
            GameContext.instance.sessionModel.currentGameType = selectedGameType;
        }

        private void OnTransitionComplete()
        {
            SceneManager.LoadScene("GameSetupMenu");
        }
    }
}