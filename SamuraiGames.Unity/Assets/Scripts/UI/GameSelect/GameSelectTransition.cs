﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameSelectTransition : MonoBehaviour
{
    public event Action OnTransitionComplete;

    public Image transitionImage;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void StartTransition()
    {
        transitionImage.DOColor(Color.black, 1f);
        transitionImage.rectTransform.DOScale(10, 1.66f).OnComplete(TransitionComplete);        
    }

    private void TransitionComplete()
    {
        if (OnTransitionComplete != null)
        {
            OnTransitionComplete();
        }
    }
}
