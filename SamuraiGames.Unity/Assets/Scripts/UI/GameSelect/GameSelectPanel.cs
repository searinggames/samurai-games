﻿using Rewired;
using SamuraiGames.Core;
using SamuraiGames.Core.Game;
using SamuraiGames.Core.Types;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


namespace SamuraiGames.Unity.UI
{
    public class GameSelectPanel : MonoBehaviour
    {
        [Range(0f, 5f)]
        public float scrollCooldown;

        private int _playerId = 0;
        private Player _playerInput;
        private List<Button> _levelButtons;
        private int _selectedButtonIndex;
        private Vector2 _inputAxis;
        private float _scrollCooldownTimer;

        void Awake()
        {
            _playerInput = ReInput.players.GetPlayer(_playerId);
            _levelButtons = this.GetComponentsInChildren<Button>().ToList();
        }

        // Use this for initialization
        void Start()
        {
            if (_levelButtons.Count > 0)
            {
                SelectButton(0);
            }
        }

        // Update is called once per frame
        void Update()
        {
            GetInput();
            ProcessInput();
        }

        public GameTypes GetSelectedGameType()
        {
            GameSelectButton gameSelectButton = _levelButtons[_selectedButtonIndex].GetComponent<GameSelectButton>();
            return gameSelectButton.gameType;
        }

        private void GetInput()
        {
            _inputAxis.y = _playerInput.GetAxis("Menu Vertical");
        }

        private void ProcessInput()
        {
            if (_inputAxis.y > 0.5)
            {
                SelectButton(_selectedButtonIndex - 1);
            }
            else if (_inputAxis.y < -0.5)
            {
                SelectButton(_selectedButtonIndex + 1);
            }
            else if (_inputAxis.y == 0 && _scrollCooldownTimer > 0)
            {
                this.StopCoroutine("RunCooldown");
                _scrollCooldownTimer = 0;
            }
        }

        private void SelectButton(int buttonIndex)
        {
            if (buttonIndex >= _levelButtons.Count)
            {
                buttonIndex = 0;
            }
            else if (buttonIndex < 0)
            {
                buttonIndex = _levelButtons.Count - 1;
            }

            // Check for an active cooldown to determine if we can keep scrolling.
            if (_scrollCooldownTimer <= 0)
            {
                _levelButtons[_selectedButtonIndex].interactable = false;
                _selectedButtonIndex = buttonIndex;
                _levelButtons[_selectedButtonIndex].interactable = true;

                this.StartCoroutine("RunCooldown");
            }
        }

        private IEnumerator RunCooldown()
        {
            _scrollCooldownTimer = scrollCooldown;
            while (_scrollCooldownTimer > 0)
            {
                _scrollCooldownTimer -= Time.deltaTime;
                yield return null;
            }

            _scrollCooldownTimer = 0;
        }
    }
}
