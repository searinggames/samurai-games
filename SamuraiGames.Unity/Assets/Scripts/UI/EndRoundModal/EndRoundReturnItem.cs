﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SamuraiGames.Unity.UI
{
    public class EndRoundReturnItem : MonoBehaviour, IMenuItem
    {
        public static event Action OnReturnPressed;

        public Color highlightColor;

        public int highlightFontSize;

        private Text _interactiveText;
        private Color _initialColor;
        private int _initialFontsize;

        void Awake()
        {
            _interactiveText = this.GetComponent<Text>();
            _initialColor = _interactiveText.color;
            _initialFontsize = _interactiveText.fontSize;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void Highlight()
        {
            _interactiveText.color = highlightColor;
            _interactiveText.fontSize = highlightFontSize;
        }

        public void Unhighlight()
        {
            _interactiveText.color = _initialColor;
            _interactiveText.fontSize = _initialFontsize;
        }

        public void Press()
        {
            if (OnReturnPressed != null)
            {
                OnReturnPressed();
            }
        }
    }
}