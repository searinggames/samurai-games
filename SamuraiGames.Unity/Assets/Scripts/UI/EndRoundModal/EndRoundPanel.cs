﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;
using UnityEngine.UI;

namespace SamuraiGames.Unity.UI
{
    public class EndRoundPanel : MonoBehaviour
    {
        [Range(0f, 5f)]
        public float scrollCooldown = 0.33f;

        private Text _winText;

        private List<IMenuItem> _menuItems;
        private int _playerId = 0;
        private Player _playerInput;
        private int _selectedItemIndex;
        private Vector2 _inputAxis;
        private float _scrollCooldownTimer;

        void Awake()
        {
            _winText = this.transform.Find("WinText").GetComponent<Text>();
            _playerInput = ReInput.players.GetPlayer(_playerId);
            _menuItems = this.GetComponentsInChildren<IMenuItem>().ToList();
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            GetInput();
            ProcessInput();
        }

        public void Show(string playerName)
        {
            _winText.text = playerName + " wins!";
            _winText.transform.DOShakePosition(0.33f, 3f, 6).SetLoops(-1);

            Vector3 scale = this.transform.localScale;
            scale.x = 0;
            this.transform.localScale = scale;
            this.gameObject.SetActive(true);
            _playerInput.controllers.maps.SetAllMapsEnabled(false);            
            this.transform.DOScaleX(1f, 1f).SetEase(Ease.OutExpo).OnComplete(ShowComplete);            
        }

        public void Hide()
        {
            _playerInput.controllers.maps.SetMapsEnabled(false, "Menu");
            _inputAxis = Vector2.zero;
            this.gameObject.SetActive(false);
            
        }

        private void GetInput()
        {
            _inputAxis.y = _playerInput.GetAxis("Menu Vertical");
        }

        private void ProcessInput()
        {
            // Check scrolling.
            if (_inputAxis.y > 0.5)
            {
                SelectItem(_selectedItemIndex - 1);
            }
            else if (_inputAxis.y < -0.5)
            {
                SelectItem(_selectedItemIndex + 1);
            }
            else if (_inputAxis.y == 0 && _scrollCooldownTimer > 0)
            {
                this.StopCoroutine("RunCooldown");
                _scrollCooldownTimer = 0;
            }

            // Check for button press.
            if (_playerInput.GetButtonDown("Confirm"))
            {
                _menuItems[_selectedItemIndex].Press();
            }
        }

        void SelectItem(int itemIndex)
        {
            if (itemIndex >= _menuItems.Count)
            {
                itemIndex = 0;
            }
            else if (itemIndex < 0)
            {
                itemIndex = _menuItems.Count - 1;
            }

            // Check for an active cooldown to determine if we can keep scrolling.
            if (_scrollCooldownTimer <= 0)
            {
                _menuItems[_selectedItemIndex].Unhighlight();
                _selectedItemIndex = itemIndex;
                _menuItems[_selectedItemIndex].Highlight();

                if (this.gameObject.activeSelf)
                {
                    this.StartCoroutine("RunCooldown");
                }
            }
        }

        private IEnumerator RunCooldown()
        {
            _scrollCooldownTimer = scrollCooldown;
            while (_scrollCooldownTimer > 0)
            {
                _scrollCooldownTimer -= Time.deltaTime;
                yield return null;
            }

            _scrollCooldownTimer = 0;
        }

        private void ShowComplete()
        {
            if (_menuItems.Count > 0)
            {
                SelectItem(0);
            }

            _playerInput.controllers.maps.SetMapsEnabled(true, "Menu");
        }
    }
}