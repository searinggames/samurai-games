﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SamuraiGames.Unity.UI
{
    public class GameSetupPanel : MonoBehaviour
    {
        [Range(0f, 5f)]
        public float scrollCooldown;

        [HideInInspector]
        public RoundsSetupItem roundSetupItem;

        [HideInInspector]
        public DifficultySetupItem difficultySetupItem;

        // A list of the defined setup item properties, designed to make traversing
        // the options simple.
        private List<IMenuItem> _setupItems;
        private int _playerId = 0;
        private Player _playerInput;
        private int _selectedItemIndex;
        private Vector2 _inputAxis;
        private float _scrollCooldownTimer;

        void Awake()
        {
            _playerInput = ReInput.players.GetPlayer(_playerId);
            roundSetupItem = this.GetComponentInChildren<RoundsSetupItem>();
            difficultySetupItem = this.GetComponentInChildren<DifficultySetupItem>();
            _setupItems = this.GetComponentsInChildren<IMenuItem>().ToList();            
        }

        // Use this for initialization
        void Start()
        {
            if (_setupItems.Count > 0)
            {
                SelectItem(0);
            }
        }

        // Update is called once per frame
        void Update()
        {
            GetInput();
            ProcessInput();
        }

        private void GetInput()
        {
            _inputAxis.y = _playerInput.GetAxis("Menu Vertical");
        }

        private void ProcessInput()
        {
            // Check scrolling.
            if (_inputAxis.y > 0.5)
            {
                SelectItem(_selectedItemIndex - 1);
            }
            else if (_inputAxis.y < -0.5)
            {
                SelectItem(_selectedItemIndex + 1);
            }
            else if (_inputAxis.y == 0 && _scrollCooldownTimer > 0)
            {
                this.StopCoroutine("RunCooldown");
                _scrollCooldownTimer = 0;
            }

            // Check for button press.
            if (_playerInput.GetButtonDown("Confirm"))
            {
                _setupItems[_selectedItemIndex].Press();
            }
        }

        void SelectItem(int itemIndex)
        {
            if (itemIndex >= _setupItems.Count)
            {
                itemIndex = 0;
            }
            else if (itemIndex < 0)
            {
                itemIndex = _setupItems.Count - 1;
            }

            // Check for an active cooldown to determine if we can keep scrolling.
            if (_scrollCooldownTimer <= 0)
            {
                _setupItems[_selectedItemIndex].Unhighlight();
                _selectedItemIndex = itemIndex;
                _setupItems[_selectedItemIndex].Highlight();

                this.StartCoroutine("RunCooldown");
            }
        }

        private IEnumerator RunCooldown()
        {
            _scrollCooldownTimer = scrollCooldown;
            while (_scrollCooldownTimer > 0)
            {
                _scrollCooldownTimer -= Time.deltaTime;
                yield return null;
            }

            _scrollCooldownTimer = 0;
        }
    }
}