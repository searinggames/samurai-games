﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SamuraiGames.Unity.UI
{
    public class GameSetupPlayButton : MonoBehaviour, IMenuItem
    {
        public static event Action OnGameSetupPlayPressed;

        public Color highlightColor;

        public int highlightFontSize;

        private Button _button;
        private Text _buttonText;
        private Color _initialTextColor;
        private int _initialFontsize;

        void Awake()
        {
            _button = this.GetComponent<Button>();
            _buttonText = this.GetComponentInChildren<Text>();
            _initialTextColor = _buttonText.color;
            _initialFontsize = _buttonText.fontSize;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void Highlight()
        {
            _button.interactable = true;
            _buttonText.color = highlightColor;
            _buttonText.fontSize = highlightFontSize;
        }

        public void Unhighlight()
        {
            _button.interactable = false;
            _buttonText.color = _initialTextColor;
            _buttonText.fontSize = _initialFontsize;
        }

        public void Press()
        {
            if (OnGameSetupPlayPressed != null)
            {
                OnGameSetupPlayPressed();
            }
        }
    }
}