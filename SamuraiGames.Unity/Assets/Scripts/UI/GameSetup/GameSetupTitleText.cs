﻿using SamuraiGames.Core.Types;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SamuraiGames.Unity.UI
{
    public class GameSetupTitleText : MonoBehaviour
    {
        public Color quickSliceColor;
        public Color ninjaJumpColor;

        private Text _text;
        private Color _initialColor;

        void Awake()
        {
            _text = this.GetComponent<Text>();
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void SetGameTitleText(GameTypes currentGameType, string gameTitle)
        {
            _text.text = gameTitle + " setup!";
            _text.color = GetTitleColor(currentGameType);          
        }

        private Color GetTitleColor(GameTypes currentGameType)
        {
            Color titleColor = _text.color;

            if (currentGameType == GameTypes.QUICK_SLICE)
            {
                titleColor = quickSliceColor;
            }
            else if (currentGameType == GameTypes.NINJA_JUMP)
            {
                titleColor = ninjaJumpColor;
            }

            return titleColor;
        }
    }
}