﻿using SamuraiGames.Core.Types;
using SamuraiGames.Unity.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace SamuraiGames.Unity.UI
{
    public class DifficultySetupItem : MonoBehaviour, IMenuItem
    {
        public DifficultyTypes selectedDifficulty;

        public Color easyHighlightColor;

        public Color mediumHighlightColor;

        public Color hardHighlightColor;

        public int highlightFontSize;

        private Text _interactiveText;
        private Color _initialColor;
        private int _initialFontsize;

        void Awake()
        {
            _interactiveText = this.transform.Find("DifficultySettingText").GetComponent<Text>();
            _initialColor = _interactiveText.color;
            _initialFontsize = _interactiveText.fontSize;
        }

        // Use this for initialization
        void Start()
        {
            //Init(selectedDifficulty);
            //Highlight();
        }

        public void Init(DifficultyTypes difficulty)
        {
            selectedDifficulty = difficulty;
            SetDifficultyText(selectedDifficulty);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void Highlight()
        {
            _interactiveText.fontSize = highlightFontSize;
            SetDifficultyText(selectedDifficulty);
            SetDifficultyVisual(selectedDifficulty);
        }

        public void Unhighlight()
        {
            _interactiveText.color = _initialColor;
            _interactiveText.fontSize = _initialFontsize;
        }

        public void Press()
        {
            // Go up to the next difficulty value.
            if (selectedDifficulty == DifficultyTypes.EASY)
            {
                selectedDifficulty = DifficultyTypes.NORMAL;
            }
            else if (selectedDifficulty == DifficultyTypes.NORMAL)
            {
                selectedDifficulty = DifficultyTypes.HARD;
            }
            else
            {
                selectedDifficulty = DifficultyTypes.EASY;
            }

            SetDifficultyText(selectedDifficulty);
            SetDifficultyVisual(selectedDifficulty);
        }

        private void SetDifficultyText(DifficultyTypes difficulty)
        {
            if (difficulty == DifficultyTypes.EASY)
            {
                _interactiveText.text = "EASY";
            }
            else if (difficulty == DifficultyTypes.NORMAL)
            {
                _interactiveText.text = "MEDIUM";
            }
            else
            {
                _interactiveText.text = "HARD";
            }
        }

        private void SetDifficultyVisual(DifficultyTypes difficulty)
        {
            if (difficulty == DifficultyTypes.EASY)
            {
                _interactiveText.color = easyHighlightColor;
            }
            else if (difficulty == DifficultyTypes.NORMAL)
            {
                _interactiveText.color = mediumHighlightColor;
            }
            else
            {
                _interactiveText.color = hardHighlightColor;
            }
        }        
    }
}