﻿using Rewired;
using SamuraiGames.Core.Game;
using SamuraiGames.Core.Minigames;
using SamuraiGames.Core.Models;
using SamuraiGames.Core.Types;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SamuraiGames.Unity.UI
{
    public class GameSetupController : MonoBehaviour
    {
        private int _playerId = 0;
        private Player _playerInput;
        private GameSetupPanel _gameSetupPanel;
        private GameSetupTitleText _gameSetupTitleText;

        private MinigameSetupModel _setupModel;
        private GameTypes _currentGameType;

        void Awake()
        {
            _playerInput = ReInput.players.GetPlayer(_playerId);
            _gameSetupPanel = this.GetComponentInChildren<GameSetupPanel>();
            _gameSetupTitleText = this.GetComponentInChildren<GameSetupTitleText>();
        }

        // Use this for initialization
        void Start()
        {
            // Manually setting the selected game type here only for testing.
            //GameContext.instance.sessionModel.currentGameType = Core.Types.GameTypes.QUICK_SLICE;

            _playerInput.controllers.maps.SetAllMapsEnabled(false);
            _playerInput.controllers.maps.SetMapsEnabled(true, "Menu");

            _setupModel = GameContext.instance.GetCurrentSetupModel();
            _currentGameType = GameContext.instance.sessionModel.currentGameType;

            string gameTitleText = MinigameTypeToTitleMap.titleMap[_currentGameType];
            _gameSetupTitleText.SetGameTitleText(_currentGameType, gameTitleText);            
            _gameSetupPanel.roundSetupItem.Init(_setupModel.numRounds, _setupModel.maxRounds);
            _gameSetupPanel.difficultySetupItem.Init(_setupModel.difficulty);
        }

        void OnEnable()
        {
            GameSetupPlayButton.OnGameSetupPlayPressed += OnGameSetupPlayPressed;
        }

        void OnDisable()
        {
            GameSetupPlayButton.OnGameSetupPlayPressed -= OnGameSetupPlayPressed;
        }

        // Update is called once per frame
        void Update()
        {
            ProcessInput();
        }

        private void ProcessInput()
        {            
            if (_playerInput.GetButtonDown("Cancel"))
            {
                // NOTE: This scene name will likely change later on.
                SceneManager.LoadScene("TestMenu");
            }
            else if (_playerInput.GetButtonDown("Start"))
            {
                GoToGame();
            }            
        }

        private void OnGameSetupPlayPressed()
        {
            GoToGame();
        }

        private void GoToGame()
        {
            _playerInput.controllers.maps.SetAllMapsEnabled(false);

            // Update the setup model with any of the new value from the UI.
            _setupModel.numRounds = _gameSetupPanel.roundSetupItem.selectedRounds;
            _setupModel.difficulty = _gameSetupPanel.difficultySetupItem.selectedDifficulty;

            // TODO: Transition to the minigame scene. 
            // This immediate navigation is temporary and will move to a transition callback.
            string sceneName;
            GameSceneMap.sceneMap.TryGetValue(GameContext.instance.sessionModel.currentGameType, out sceneName);

            if (!string.IsNullOrEmpty(sceneName))
            {
                SceneManager.LoadScene(sceneName);
            }           
        }
    }
}