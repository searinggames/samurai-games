﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SamuraiGames.Unity.UI
{
    public class RoundsSetupItem : MonoBehaviour, IMenuItem
    {
        public int selectedRounds;

        public int maxSelectableRounds;

        public Color highlightColor;

        public int highlightFontSize;

        private Text _interactiveText;
        private Color _initialColor;
        private int _initialFontsize;    

        void Awake()
        {
            _interactiveText = this.transform.Find("RoundsSettingText").GetComponent<Text>();
            _initialColor = _interactiveText.color;
            _initialFontsize = _interactiveText.fontSize;
        }
        
        // Use this for initialization
        void Start()
        {
            // Rounds should always be an odd number.
            if (selectedRounds % 2 == 0)
            {
                selectedRounds++;
            }

            if (selectedRounds > maxSelectableRounds)
            {
                selectedRounds = maxSelectableRounds;
            }
        }

        public void Init(int numRounds, int roundsMax)
        {
            selectedRounds = numRounds;
            maxSelectableRounds = roundsMax;
            SetRoundText(selectedRounds);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void Highlight()
        {
            _interactiveText.color = highlightColor;
            _interactiveText.fontSize = highlightFontSize;
        }

        public void Unhighlight()
        {
            _interactiveText.color = _initialColor;
            _interactiveText.fontSize = _initialFontsize;
        }

        public void Press()
        {
            // Go up to the next round value.
            selectedRounds += 2;
            
            if (selectedRounds > maxSelectableRounds)
            {
                selectedRounds = 1;
            }            

            SetRoundText(selectedRounds);
        }        

        void SetRoundText(int numRounds)
        {
            _interactiveText.text = numRounds.ToString();
        }
    }
}