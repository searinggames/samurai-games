﻿using SamuraiGames.Core.Game;
using SamuraiGames.Core.Models;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRoot : MonoBehaviour
{ 
    void Awake()
    {
        DontDestroyOnLoad(this.transform.gameObject);
        InitContext();
    }

    // Use this for initialization
    void Start()
    {

    }

    /// <summary>
    /// NOTE: We may want to put all this logic in OnDestroy instead.
    /// I don't see an issue with OnApplicationQuit, but just in case...
    /// </summary>
    public void OnApplicationQuit()
    {        
        GameContext.instance.Shutdown();
    }

    private void InitContext()
    {
        GameContext.instance.Init();
        GameContext.instance.sessionModel = new SessionModel();
    }
}