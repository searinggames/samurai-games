﻿using SamuraiGames.Core.Game;
using SamuraiGames.Core.Types;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SamuraiGames.Unity
{
    public class GameSceneMap
    {
        public static Dictionary<GameTypes, string> sceneMap = new Dictionary<GameTypes, string>()
        {
            { GameTypes.NONE, "" },
            { GameTypes.KATANA_CROSS, "KatanaCross" },
            { GameTypes.KENDO_CLASH, "KendoClash" },
            { GameTypes.NINJA_JUMP, "NinjaJump" },
            { GameTypes.QUICK_SLICE, "QuickSlice" },
            { GameTypes.SAMURAI_CHARGE, "SamuraiCharge" },
            { GameTypes.SLASH_AND_DASH, "SlashAndDash" }
        };
    }
}