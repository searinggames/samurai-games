﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SamuraiGames.Unity.QuickSlice
{
    [CreateAssetMenu]
    public class QuickSliceRoundSettings : ScriptableObject
    {        
        public float roundTimeMin;

        public float roundTimeMax;

        public int maxDistractions;
    }
}