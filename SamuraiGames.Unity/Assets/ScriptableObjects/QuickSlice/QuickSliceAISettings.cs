﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SamuraiGames.Unity.QuickSlice
{
    [CreateAssetMenu]
    public class QuickSliceAISettings : ScriptableObject
    {
        public float reactionTimeMin;

        public float reactionTimeMax;

        public float drawFrequency;
    }
}