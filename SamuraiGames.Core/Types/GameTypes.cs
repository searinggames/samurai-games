﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.Core.Types
{
    public enum GameTypes
    {
        NONE,
        SLASH_AND_DASH,
        KENDO_CLASH,
        NINJA_JUMP,
        KATANA_CROSS,
        QUICK_SLICE,
        SAMURAI_CHARGE
    }
}
