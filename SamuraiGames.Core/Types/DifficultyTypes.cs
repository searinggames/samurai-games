﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.Core.Types
{
    public enum DifficultyTypes
    {
        EASY,
        NORMAL,
        HARD
    }
}
