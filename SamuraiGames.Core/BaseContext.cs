﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.Core
{
    public abstract class BaseContext
    {
        public abstract void Init();

        public abstract void Start();

        public abstract void Shutdown();

        protected abstract void LoadGameModel();    
    }
}
