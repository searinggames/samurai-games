﻿using SamuraiGames.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.Core.Models
{
    public class MinigameSetupModel
    {
        public int numRounds { get; set; }

        public int maxRounds { get; set; }

        public DifficultyTypes difficulty { get; set; }

        public MinigameSetupModel() { }

        public MinigameSetupModel(MinigameSetupModel other)
        {
            numRounds = other.numRounds;
            maxRounds = other.maxRounds;
            difficulty = other.difficulty;
        }

        public MinigameSetupModel Clone()
        {
            return new MinigameSetupModel(this);
        }
    }
}
