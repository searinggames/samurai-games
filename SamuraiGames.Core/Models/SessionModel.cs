﻿using SamuraiGames.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.Core.Models
{
    public class SessionModel : ISessionModel
    {
        public GameTypes currentGameType { get; set; }
    }
}
