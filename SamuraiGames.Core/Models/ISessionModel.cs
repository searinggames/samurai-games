﻿using SamuraiGames.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.Core.Models
{
    public interface ISessionModel
    {
        GameTypes currentGameType { get; set; }
    }
}
