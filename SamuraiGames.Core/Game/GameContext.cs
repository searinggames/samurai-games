﻿using SamuraiGames.Core.Models;
using SamuraiGames.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.Core.Game
{
    public sealed class GameContext
    {
        public static GameContext instance { get { return _instance; } }
        private static readonly GameContext _instance = new GameContext();
        private GameContext() { }
        
        /// <summary>
        /// All values used just for a single play session are stored here.
        /// </summary>
        public ISessionModel sessionModel;

        // Putting the setup map outside of the session in case want to retain it
        // across play sessions.
        public Dictionary<GameTypes, MinigameSetupModel> minigameSetupMap;

        public void Init()
        {
            // TODO: May want to save/load this minigame setup map to/from disc between sessions.
            InitSetupMap();
        }

        public void Shutdown()
        {

        }

        public MinigameSetupModel GetCurrentSetupModel()
        {
            if (minigameSetupMap.ContainsKey(sessionModel.currentGameType))
            {
                return minigameSetupMap[sessionModel.currentGameType];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Build the setup map, with all the defaults.
        /// </summary>
        private void InitSetupMap()
        {
            MinigameSetupModel setupModelDefaultPrototype = new MinigameSetupModel()
            {
                numRounds = 3,
                maxRounds = 9,
                difficulty = DifficultyTypes.NORMAL
            };

            minigameSetupMap = new Dictionary<GameTypes, MinigameSetupModel>()
            {
                { GameTypes.KATANA_CROSS, setupModelDefaultPrototype.Clone() },
                { GameTypes.KENDO_CLASH, setupModelDefaultPrototype.Clone() },
                { GameTypes.NINJA_JUMP, setupModelDefaultPrototype.Clone() },
                { GameTypes.QUICK_SLICE, setupModelDefaultPrototype.Clone() },
                { GameTypes.SAMURAI_CHARGE, setupModelDefaultPrototype.Clone() },
                { GameTypes.SLASH_AND_DASH, setupModelDefaultPrototype.Clone() }
            };

        }
    }
}
