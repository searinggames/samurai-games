﻿using SamuraiGames.Core.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.Core.Events
{
    /// <summary>
    /// Dispatches all events sent by the engine to the client.
    /// </summary>
    public class EngineDispatcher : IDispatcher
    {
        public void Dispatch(Action action)
        {
            if (action != null)
            {
                action();
            }
        }

        public void Dispatch<TArg>(Action<TArg> action, TArg arg)
        {
            if (action != null)
            {
                action(arg);
            }
        }
    }
}
