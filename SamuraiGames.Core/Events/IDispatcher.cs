﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.Core.Events
{
    public interface IDispatcher
    {
        void Dispatch(Action action);

        void Dispatch<TArg>(Action<TArg> action, TArg arg);
    }
}
