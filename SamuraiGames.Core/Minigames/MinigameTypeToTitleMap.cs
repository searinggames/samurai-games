﻿using SamuraiGames.Core.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.Core.Minigames
{
    public class MinigameTypeToTitleMap
    {
        public static Dictionary<GameTypes, string> titleMap = new Dictionary<GameTypes, string>()
        {
            { GameTypes.NONE, "" },
            { GameTypes.KATANA_CROSS, "Katana Cross" },
            { GameTypes.KENDO_CLASH, "Kendo Clash" },
            { GameTypes.NINJA_JUMP, "Ninja Jump" },
            { GameTypes.QUICK_SLICE, "Quick Slice" },
            { GameTypes.SAMURAI_CHARGE, "Samurai Charge" },
            { GameTypes.SLASH_AND_DASH, "Slash & Dash" }
        };
    }
}
