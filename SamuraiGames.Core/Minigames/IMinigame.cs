﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.Core.Minigames
{
    public interface IMinigame
    {
        void Init();

        void Start();

        void Shutdown();
    }
}
