﻿using SamuraiGames.Core.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.NinjaJump.Events
{
    public class EngineEvents : EventsContainer
    {
        public EngineEvents(IDispatcher dispatcher) : base(dispatcher)
        {
        }

        /// <summary>
        /// The game's initialization is complete.
        /// </summary>
        public event Action MinigameReady;
        internal void OnMinigameReady() { _dispatcher.Dispatch(MinigameReady); }
    }
}
