﻿using SamuraiGames.Core;
using SamuraiGames.Core.Events;
using SamuraiGames.NinjaJump.Events;
using SamuraiGames.NinjaJump.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SamuraiGames.NinjaJump
{
    public sealed class NinjaJumpGame : BaseContext
    {
        public static NinjaJumpGame instance { get { return _instance; } }
        private static readonly NinjaJumpGame _instance = new NinjaJumpGame();
        private NinjaJumpGame() { }

        // Bindanble Interfaces
        // Interfaces for the client to bind, might move to a generic container class to explicitly show what should be bound.

        // /Bindable Interfaces

        public GameModel gameModel { get; internal set; }

        public RoundModel roundModel { get; internal set; }

        public EngineEvents engineEvents { get { return _engineEvents; } }
        private EngineEvents _engineEvents;

        public ClientEvents clientEvents { get { return _clientEvents; } }
        private ClientEvents _clientEvents;

        private IDispatcher _engineDispatcher;
        private IDispatcher _clientDispatcher;

        public override void Init()
        {
            _engineDispatcher = new EngineDispatcher();
            _engineEvents = new EngineEvents(_engineDispatcher);

            _clientDispatcher = new ClientDispatcher();
            _clientEvents = new ClientEvents(_clientDispatcher);
        }

        public override void Shutdown()
        {
            
        }

        public override void Start()
        {
            
        }

        protected override void LoadGameModel()
        {
            
        }
    }
}
